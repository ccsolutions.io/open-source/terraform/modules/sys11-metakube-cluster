terraform {
  required_version = "1.7.3"
  required_providers {
    metakube = {
      source  = "syseleven/metakube"
      version = "5.3.4"
    }
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "2.0.0"
    }
  }
}
