# Runbook for `sys11-metakube-cluster` Tofu Module

## Introduction

This runbook provides the necessary steps and guidelines to set up, configure, and maintain the `sys11-metakube-cluster` Tofu module. This module is designed to facilitate the creation and management of a SysEleven Metakube Kubernetes Cluster.

## Pre-requisites

Ensure you have the following requirements met before you proceed:

- Tofu ~1.7.0
- Metakube 5.3.4
- openstack 2.0.0

### Environment Variables

Ensure to set up the necessary environment variables. Here are some of the essential variables:

- `METAKUBE_PROJECT_ID`: Your Metakube project ID
- `METAKUBE_TOKEN`: Token for the Metakube project
- `SYSELEVEN_AUTH_REALM`: Authentication realm, default is "ccsolutions"
- `SYSELEVEN_CREDENTIAL_ID`: Credential ID for SysEleven OpenStack project
- `SYSELEVEN_CREDENTIAL_SECRET`: Credential secret for SysEleven OpenStack project

## Setup and Configuration

### Step 1: Clone the Repository

Clone the repository to your local system:

```sh
git clone https://gitlab.com/ccsolutions.io/open-source/terraform/modules/sys11-metakube-cluster.git
```

### Step 2: Configure Tofu

Navigate to the project directory and initialize Tofu:

```sh
cd sys11-metakube-cluster
tofu init
```

### Step 3: Configure Variables

Open the `variables.tf` file and configure the necessary variables as per your requirements.

### Step 4: Apply the Configuration

Apply the Tofu configuration to create the resources:

```sh
tofu apply
```

## Maintenance

### Updating the Module

To update the module to the latest version, pull the latest changes from the GitLab repository and re-apply the Tofu configuration:

```sh
git pull
tofu apply
```

### Viewing Outputs

To view the outputs after applying the configuration, use the following command:

```sh
tofu output
```

## Troubleshooting

If you encounter any issues during the setup or maintenance process, refer to the Tofu and Metakube documentation or check the README file in the repository for guidance.

## Conclusion

This runbook outlines the basic steps to set up and maintain the `sys11-metakube-cluster` Tofu module. Always ensure to keep the module updated to the latest version to incorporate any new features or bug fixes.

For detailed information on the inputs and outputs, refer to the README file in the repository.
