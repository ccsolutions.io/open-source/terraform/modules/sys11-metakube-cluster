variable "environment" {
  description = "The name of the Environment"
  type        = string
}
variable "project" {
  description = "The name of the Project/Customer"
  type        = string
}
variable "openstack_subnet_cidr" {
  description = "(Optional) CIDR representing IP range for the version. You can omit this option if you are creating a subnet from a subnet pool."
  type        = string
  default     = "192.168.1.0/24"
}
variable "openstack_subnet_allocation_pool_start" {
  description = "OpenStack Network Range, first IP of the Pool"
  type        = string
  default     = "192.168.1.2"
}
variable "openstack_subnet_allocation_pool_end" {
  description = "OpenStack Network Range, last IP of the Pool"
  type        = string
  default     = "192.168.1.254"
}
variable "openstack_dns_nameserver" {
  description = "List of DNS nameserver to be used"
  type        = list(string)
  default     = ["37.123.105.116", "37.123.105.117"]
}
variable "floating_ip_pool" {
  description = "Floating IP pool to use for all worker nodes"
  type        = string
  default     = "ext-net"
}
variable "openstack_images_distro" {
  description = "The name in the distro"
  type        = string
  default     = "ubuntu"
}
variable "openstack_images_version" {
  description = "The version image in the distro"
  type        = string
  default     = "20.04"
}
