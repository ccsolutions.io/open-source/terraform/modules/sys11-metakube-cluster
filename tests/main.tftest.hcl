variables {
  environment              = "development"
  kubernetes_version       = "1.31.2"
  project                  = "captain-america"
  openstack_images_distro  = "ubuntu"
  openstack_images_version = "24.04"
  node_image               = "Ubuntu Noble 24.04 (2024-11-08)"
  node_min_replicas        = 1
  node_max_replicas        = 1
  node_flavor              = "m1.small"
  ssh_username             = "ligthon"
  use_floating_ip          = false
  syseleven_dc_name        = "syseleven-fes1"
  metakube_project_id      = "tf8gvtwzfr"
  external_labels = {
    "team"     = "avengers"
    "costcode" = "123456"
    # Deliberately override default 'app' label
    "app" = "overridden-app"
    # Deliberately override default 'env' label
    "env" = "overridden-env"
  }
}

run "setup_network" {
  command = apply

  variables {
    openstack_subnet_cidr                  = "192.254.254.0/24"
    openstack_subnet_allocation_pool_start = "192.254.254.20"
    openstack_subnet_allocation_pool_end   = "192.254.254.254"
    openstack_dns_nameserver               = ["37.123.105.116", "8.8.8.8"]
  }

  module {
    source = "./tests/helper"
  }

}

run "create_cluster" {
  command = plan

  variables {
    openstack_networking_secgroup      = run.setup_network.openstack_networking_secgroup
    openstack_networking_network       = run.setup_network.openstack_networking_network
    openstack_networking_subnet        = run.setup_network.openstack_networking_subnet
    openstack_networking_secgroup_rule = run.setup_network.openstack_networking_secgroup_rule
    openstack_compute_servergroup      = run.setup_network.openstack_compute_servergroup
  }

  # MetaKube Cluster assertions
  assert {
    condition     = metakube_cluster.cluster.name == "k8s-development-captain-america-syseleven-fes1"
    error_message = "Cluster name does not match expected value"
  }

  assert {
    condition     = metakube_cluster.cluster.dc_name == var.syseleven_dc_name
    error_message = "Data center name does not match expected value"
  }

  assert {
    condition     = metakube_cluster.cluster.spec[0].version == var.kubernetes_version
    error_message = "Kubernetes version does not match expected value"
  }

  # Node pool assertions
  assert {
    condition     = metakube_node_deployment.node_pool.name == "node-pool-development-captain-america-syseleven-fes1"
    error_message = "Node pool Name does not match expected value"
  }

  assert {
    condition     = metakube_node_deployment.node_pool.spec[0].min_replicas == var.node_min_replicas
    error_message = "Node pool min replicas does not match expected value"
  }

  assert {
    condition     = metakube_node_deployment.node_pool.spec[0].max_replicas == var.node_max_replicas
    error_message = "Node pool min replicas does not match expected value"
  }

  # Test that external labels successfully override default labels
  assert {
    condition     = metakube_cluster.cluster.labels["app"] == "overridden-app"
    error_message = "External label 'app' did not override default label"
  }

  assert {
    condition     = metakube_cluster.cluster.labels["env"] == "overridden-env"
    error_message = "External label 'env' did not override default label"
  }

  # Test that non-overridden default labels remain intact
  assert {
    condition     = metakube_cluster.cluster.labels["cluster"] == local.cluster_name
    error_message = "Non-overridden default label 'cluster' was modified"
  }

  # Test that new external labels are present
  assert {
    condition     = metakube_cluster.cluster.labels["team"] == "avengers"
    error_message = "External label 'team' was not added correctly"
  }

  assert {
    condition     = metakube_cluster.cluster.labels["costcode"] == "123456"
    error_message = "External label 'costcode' was not added correctly"
  }
}