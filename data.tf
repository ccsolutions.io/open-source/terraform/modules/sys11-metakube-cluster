data "openstack_images_image_v2" "image" {
  most_recent = true
  visibility  = "public"
  properties = {
    os_distro  = var.openstack_images_distro
    os_version = var.openstack_images_version
  }
}