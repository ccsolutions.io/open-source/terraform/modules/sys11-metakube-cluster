resource "metakube_sshkey" "ssh_keys" {
  project_id = var.metakube_project_id
  name       = var.ssh_username
  public_key = var.ssh_keys
}
resource "metakube_cluster" "cluster" {
  name       = local.cluster_name
  dc_name    = var.syseleven_dc_name
  project_id = var.metakube_project_id
  sshkeys    = [metakube_sshkey.ssh_keys.id]
  labels     = local.merged_labels
  spec {
    enable_ssh_agent = true
    version          = var.kubernetes_version
    cloud {
      openstack {
        application_credentials {
          id     = var.syseleven_credential_id
          secret = var.syseleven_credential_secret
        }
        floating_ip_pool = var.floating_ip_pool
        security_group   = var.openstack_networking_secgroup
        network          = var.openstack_networking_network
        subnet_id        = var.openstack_networking_subnet
        subnet_cidr      = var.openstack_networking_secgroup_rule
        server_group_id  = var.openstack_compute_servergroup
      }
    }
    audit_logging     = var.audit_logging_enabled
    pod_node_selector = var.pod_node_selector
    services_cidr     = var.metakube_services_cidr
    pods_cidr         = var.pods_cidr

    syseleven_auth {
      realm = var.syseleven_auth_realm
    }
  }

  depends_on = [
    metakube_sshkey.ssh_keys,
    var.openstack_networking_secgroup,
    var.openstack_networking_network,
    var.openstack_networking_subnet,
    var.openstack_networking_secgroup_rule,
    var.openstack_compute_servergroup
  ]

  timeouts {
    create = var.cluster_create_timeouts
    update = var.cluster_update_timeouts
    delete = var.cluster_delete_timeouts
  }

  lifecycle {
    prevent_destroy = true
  }
}
resource "metakube_node_deployment" "node_pool" {
  cluster_id = metakube_cluster.cluster.id
  name       = local.node_pool_name

  spec {
    min_replicas = var.node_min_replicas
    max_replicas = var.node_max_replicas

    template {
      labels = {
        "app"     = var.project
        "env"     = var.environment
        "cluster" = local.cluster_name
      }

      cloud {
        openstack {
          flavor                       = var.node_flavor
          disk_size                    = var.node_disk_size
          image                        = var.node_image
          use_floating_ip              = var.use_floating_ip
          instance_ready_check_period  = var.instance_ready_check_period
          instance_ready_check_timeout = var.instance_ready_check_timeout
          tags = {
            "app"     = var.project
            "env"     = var.environment
            "cluster" = local.cluster_name
          }
        }
      }
      operating_system {
        ubuntu {
          dist_upgrade_on_boot = var.dist_upgrade_on_boot
        }
      }
      versions {
        kubelet = var.kubernetes_version
      }
    }
  }
  timeouts {
    create = var.node_pool_create_timeouts
    update = var.node_pool_update_timeouts
    delete = var.node_pool_delete_timeouts
  }
}
