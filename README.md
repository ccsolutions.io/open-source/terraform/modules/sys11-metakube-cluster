# Syseleven Metakube Cluster module

This module provisions a Kubernetes cluster using Syseleven's Metakube service. It sets up the necessary OpenStack resources and configures the Kubernetes cluster according to the specified parameters.

## Features

- Creates a Metakube Kubernetes cluster on Syseleven's OpenStack infrastructure
- Configures a node pool with customizable specifications
- Supports SSH key management for cluster access
- Allows fine-tuning of cluster and node pool settings

## Security Scanning

This module includes security scanning as part of its CI/CD pipeline:

1. **Secret Detection**: Utilizes GitLab's built-in Secret Detection template to identify and prevent accidental commit of secrets.

2. **SAST (Static Application Security Testing) for Infrastructure-as-Code**: Employs GitLab's SAST-IaC template to analyze Terraform/OpenTofu code for potential security issues.

These scans are automatically run as part of the GitLab CI pipeline, helping to ensure the security and integrity of the infrastructure code.

### Viewing Scan Results

- **Secret Detection**: Results can be found in the GitLab CI/CD pipeline under the "secret_detection" job. You can also view detected secrets inside the Pipeline tab "Security".

- **SAST-IaC**: Results are available in the GitLab CI/CD pipeline under the "kics-iac-sast" job. You can also view detected secrets inside the Pipeline tab "Security".

To access these results:
1. Go to your Pipeline in GitLab.
2. Navigate to "Security" tab.
3. View the details of the scan results.

## Usage

To use this module, include it in your Terraform configuration and provide the required variables:

```hcl
module "syseleven_metakube_cluster" {
  source = "path/to/this/module"

  environment                    = "production"
  project                        = "my-project"
  syseleven_credential_id        = "your-credential-id"
  syseleven_credential_secret    = "your-credential-secret"
  syseleven_dc_name              = "your-datacenter"
  metakube_project_id            = "your-metakube-project-id"
  openstack_networking_secgroup  = "your-security-group"
  openstack_networking_network   = "your-network"
  openstack_networking_subnet    = "your-subnet"
  openstack_networking_secgroup_rule = "your-secgroup-rule"
  openstack_compute_servergroup  = "your-server-group"
  node_flavor                    = "your-node-flavor"
  node_image                     = "your-node-image"
  openstack_images_distro        = "ubuntu"
  openstack_images_version       = "22.04"

  # Optional: Customize other variables as needed
}
```

Refer to the inputs section below for a complete list of available variables and their descriptions.

### Custom Labels

The module supports custom labels that can be merged with or override the default labels. Default labels include:
- `app`: Set to the project name
- `env`: Set to the environment name
- `cluster`: Set to the cluster name (generated)

To add custom labels or override defaults:

```hcl
module "metakube_cluster" {
  source = "path/to/this/module"

  environment                = "development"
  project                    = "myproject"

  # Custom labels
  external_labels = {
    team      = "platform"      # Adds a new label
    costcode  = "123456"       # Adds a new label
    app       = "custom-app"    # Overrides default 'app' label
    env       = "custom-env"    # Overrides default 'env' label
  }

  # ... other configurations ...
}
```

Label merging behavior:
- External labels will be merged with default labels
- If an external label has the same key as a default label, the external label's value will take precedence
- Labels are applied to both the cluster and node pool resources

## Prevent Destroy

To prevent accidental deletion of the cluster, the `prevent_destroy` lifecycle is enabled by default. If you wish to delete the cluster, you must explicitly disable this feature.

```hcl
resource "metakube_cluster" "this" {
  # ... other configurations ...

  lifecycle {
    prevent_destroy = false
  }
}
```

## Notes

- Ensure that you have the necessary permissions and credentials for Syseleven's OpenStack and Metakube services.
- Review and adjust the default values for variables like `kubernetes_version`, `node_min_replicas`, and `node_max_replicas` to suit your specific requirements.
- The module uses OpenTofu version 1.7.3 and specific versions of the Metakube and OpenStack providers. Make sure your Terraform environment is compatible.

---<!-- BEGINNING OF PRE-COMMIT-OPENTOFU DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | 1.7.3 |
| <a name="requirement_metakube"></a> [metakube](#requirement\_metakube) | 5.3.4 |
| <a name="requirement_openstack"></a> [openstack](#requirement\_openstack) | 2.0.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_metakube"></a> [metakube](#provider\_metakube) | 5.3.4 |
| <a name="provider_openstack"></a> [openstack](#provider\_openstack) | 2.0.0 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [metakube_cluster.cluster](https://registry.terraform.io/providers/syseleven/metakube/5.3.4/docs/resources/cluster) | resource |
| [metakube_node_deployment.node_pool](https://registry.terraform.io/providers/syseleven/metakube/5.3.4/docs/resources/node_deployment) | resource |
| [metakube_sshkey.ssh_keys](https://registry.terraform.io/providers/syseleven/metakube/5.3.4/docs/resources/sshkey) | resource |
| [openstack_images_image_v2.image](https://registry.terraform.io/providers/terraform-provider-openstack/openstack/2.0.0/docs/data-sources/images_image_v2) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_audit_logging_enabled"></a> [audit\_logging\_enabled](#input\_audit\_logging\_enabled) | (Optional) Enable AUDIT Logging | `bool` | `true` | no |
| <a name="input_cluster_create_timeouts"></a> [cluster\_create\_timeouts](#input\_cluster\_create\_timeouts) | (Optional) Timeout for the cluster creation | `string` | `"2h"` | no |
| <a name="input_cluster_delete_timeouts"></a> [cluster\_delete\_timeouts](#input\_cluster\_delete\_timeouts) | (Optional) Timeout for the cluster deletion | `string` | `"2h"` | no |
| <a name="input_cluster_update_timeouts"></a> [cluster\_update\_timeouts](#input\_cluster\_update\_timeouts) | (Optional) Timeout for the cluster update | `string` | `"2h"` | no |
| <a name="input_dist_upgrade_on_boot"></a> [dist\_upgrade\_on\_boot](#input\_dist\_upgrade\_on\_boot) | (Optional) Upgrade the OS on boot | `bool` | `true` | no |
| <a name="input_environment"></a> [environment](#input\_environment) | (Required) The name of the Environment | `string` | n/a | yes |
| <a name="input_external_labels"></a> [external\_labels](#input\_external\_labels) | (Optional) Additional labels to be merged with the default cluster labels | `map(string)` | `{}` | no |
| <a name="input_floating_ip_pool"></a> [floating\_ip\_pool](#input\_floating\_ip\_pool) | (Optional) Floating IP pool to use for all worker nodes | `string` | `"ext-net"` | no |
| <a name="input_instance_ready_check_period"></a> [instance\_ready\_check\_period](#input\_instance\_ready\_check\_period) | (Optional) Check instance after seconds | `string` | `"10s"` | no |
| <a name="input_instance_ready_check_timeout"></a> [instance\_ready\_check\_timeout](#input\_instance\_ready\_check\_timeout) | (Optional) Timeout of the instance check | `string` | `"200s"` | no |
| <a name="input_kubernetes_version"></a> [kubernetes\_version](#input\_kubernetes\_version) | (Optional) Supported K8S Version by Syseleven | `string` | `"1.27.10"` | no |
| <a name="input_metakube_project_id"></a> [metakube\_project\_id](#input\_metakube\_project\_id) | (Required) Reference project identifier | `string` | n/a | yes |
| <a name="input_metakube_services_cidr"></a> [metakube\_services\_cidr](#input\_metakube\_services\_cidr) | (Optional) CIDR for the Kubernetes Services | `string` | `"10.240.16.0/20"` | no |
| <a name="input_node_disk_size"></a> [node\_disk\_size](#input\_node\_disk\_size) | (Optional) Size of the HDD for the nodes | `number` | `50` | no |
| <a name="input_node_flavor"></a> [node\_flavor](#input\_node\_flavor) | (Required) Flavor to use | `string` | n/a | yes |
| <a name="input_node_image"></a> [node\_image](#input\_node\_image) | (Required) The image for the nodes | `string` | n/a | yes |
| <a name="input_node_max_replicas"></a> [node\_max\_replicas](#input\_node\_max\_replicas) | (Optional) Maximal Node Count | `number` | `5` | no |
| <a name="input_node_min_replicas"></a> [node\_min\_replicas](#input\_node\_min\_replicas) | (Optional) Minimal Node Count | `number` | `2` | no |
| <a name="input_node_pool_create_timeouts"></a> [node\_pool\_create\_timeouts](#input\_node\_pool\_create\_timeouts) | (Optional) Timeout for the node pool creation | `string` | `"4h"` | no |
| <a name="input_node_pool_delete_timeouts"></a> [node\_pool\_delete\_timeouts](#input\_node\_pool\_delete\_timeouts) | (Optional) Timeout for the node pool deletion | `string` | `"4h"` | no |
| <a name="input_node_pool_update_timeouts"></a> [node\_pool\_update\_timeouts](#input\_node\_pool\_update\_timeouts) | (Optional) Timeout for the node pool update | `string` | `"4h"` | no |
| <a name="input_openstack_compute_servergroup"></a> [openstack\_compute\_servergroup](#input\_openstack\_compute\_servergroup) | (Required) OpenStack Server Group Anti affinity | `string` | n/a | yes |
| <a name="input_openstack_images_distro"></a> [openstack\_images\_distro](#input\_openstack\_images\_distro) | (Required) The image name for the nodes | `string` | n/a | yes |
| <a name="input_openstack_images_version"></a> [openstack\_images\_version](#input\_openstack\_images\_version) | (Required) The image version for the nodes | `string` | n/a | yes |
| <a name="input_openstack_networking_network"></a> [openstack\_networking\_network](#input\_openstack\_networking\_network) | (Required) Network for the cluster nodes | `string` | n/a | yes |
| <a name="input_openstack_networking_secgroup"></a> [openstack\_networking\_secgroup](#input\_openstack\_networking\_secgroup) | (Required) Security Group for the cluster | `string` | n/a | yes |
| <a name="input_openstack_networking_secgroup_rule"></a> [openstack\_networking\_secgroup\_rule](#input\_openstack\_networking\_secgroup\_rule) | (Required) Security Group for the Node Ports | `string` | n/a | yes |
| <a name="input_openstack_networking_subnet"></a> [openstack\_networking\_subnet](#input\_openstack\_networking\_subnet) | (Required) OpenStack Subnet | `string` | n/a | yes |
| <a name="input_pod_node_selector"></a> [pod\_node\_selector](#input\_pod\_node\_selector) | (Optional) Enable POD Node Selector | `bool` | `false` | no |
| <a name="input_pods_cidr"></a> [pods\_cidr](#input\_pods\_cidr) | (Optional) CIDR for the Kubernetes PODS | `string` | `"172.25.0.0/16"` | no |
| <a name="input_project"></a> [project](#input\_project) | (Required) The name of the Project/Customer | `string` | n/a | yes |
| <a name="input_ssh_keys"></a> [ssh\_keys](#input\_ssh\_keys) | (Optional) ssh\_key for ssh user | `string` | `"ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDP70R7TEf62SMFSsZPSJqCoZdpcg69oHfpiCpGCJYaB1mKyL5sBEnbKOWKkxBrHib+EOIp/Q/L9mllDxjxStF+fK7Xyd6jecxedQkkwLoSS1Yg981fzeSZC36lyiHZx9XUhuZ/0dGkifnguFp6mtkkDJu/7Q4KFwggnMkWmW1ypItHBI8YNQa4qYoIRTnszA6mO/ad9q8ichw3Vsr2QrNyErUXJYgwfyuGX3n+NZgUDqBw29hxPwrjdGnyptO53hNZu/D/gzOYKbxwREMhhVbK0kxZe/erBr+0niZVjEMDMupS7fdpyp/0brwcrOn30690SBsmPp0EGp7tYJMaBLWrEVReVeahJAID2DHxcuGyE1WGm0creL7kFoHbsRCPiM26dRnEYJolCOifsXfxB53Hee8+VU8YYWUGxOYfj2FagQHHf33TTAF61IKVvDk/Go+h+PLRoSiZKBlIS8TQPJSZhhPniS44sLTsjBuMaL5EbW0vC5UchXH2OOR2zj35LATp06EBL6qmdQNjYB/Fe8MBThmnFK5rSHVcEWX1rXV+iSiuneuwrqOidRUAiTUR90tdqySZZLh/0rswnNLcPVZnX50GX5Iaz9IxcBtbzOPaHfmaw2sJKpJne2eK3gJBh1fs3vLs5Nhv4yajLBP7cuRlqkkmZS/5z/97fyImpngy+Q=="` | no |
| <a name="input_ssh_username"></a> [ssh\_username](#input\_ssh\_username) | (Optional) Name of the ssh user | `string` | `"lighton"` | no |
| <a name="input_syseleven_auth_realm"></a> [syseleven\_auth\_realm](#input\_syseleven\_auth\_realm) | (Optional) Syseleven Auth Realm | `string` | `"ccsolutions"` | no |
| <a name="input_syseleven_credential_id"></a> [syseleven\_credential\_id](#input\_syseleven\_credential\_id) | (Required) Credential ID for Syseleven OpenStack Project | `string` | n/a | yes |
| <a name="input_syseleven_credential_secret"></a> [syseleven\_credential\_secret](#input\_syseleven\_credential\_secret) | (Required) Credential Secret for Syseleven OpenStack Project | `string` | n/a | yes |
| <a name="input_syseleven_dc_name"></a> [syseleven\_dc\_name](#input\_syseleven\_dc\_name) | (Required) Data center name. | `string` | n/a | yes |
| <a name="input_use_floating_ip"></a> [use\_floating\_ip](#input\_use\_floating\_ip) | (Optional) Use Floating IP for the nodes | `bool` | `true` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_cluster_name"></a> [cluster\_name](#output\_cluster\_name) | Cluster name |
| <a name="output_image_version"></a> [image\_version](#output\_image\_version) | Fetch latest image\_version |
| <a name="output_kube_config"></a> [kube\_config](#output\_kube\_config) | Admin kube config raw content which can be dumped to a file using local\_file |
<!-- END OF PRE-COMMIT-OPENTOFU DOCS HOOK -->
