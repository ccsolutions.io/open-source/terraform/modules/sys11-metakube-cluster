output "kube_config" {
  description = "Admin kube config raw content which can be dumped to a file using local_file"
  value       = metakube_cluster.cluster.kube_config
}
output "cluster_name" {
  description = "Cluster name"
  value       = metakube_cluster.cluster.name
}
output "image_version" {
  description = "Fetch latest image_version"
  value       = data.openstack_images_image_v2.image.name
}
