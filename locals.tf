locals {
  cluster_name   = "k8s-${var.environment}-${var.project}-${var.syseleven_dc_name}"
  node_pool_name = "node-pool-${var.environment}-${var.project}-${var.syseleven_dc_name}"
  default_labels = {
    "app"     = var.project
    "env"     = var.environment
    "cluster" = local.cluster_name
  }
  merged_labels = merge(local.default_labels, var.external_labels)
}
