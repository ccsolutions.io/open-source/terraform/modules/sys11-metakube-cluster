variable "environment" {
  description = "(Required) The name of the Environment"
  type        = string
}
variable "project" {
  description = "(Required) The name of the Project/Customer"
  type        = string
}
variable "syseleven_credential_id" {
  description = "(Required) Credential ID for Syseleven OpenStack Project"
  type        = string
}
variable "syseleven_credential_secret" {
  description = "(Required) Credential Secret for Syseleven OpenStack Project"
  type        = string
}
variable "syseleven_dc_name" {
  description = "(Required) Data center name."
  type        = string
}
variable "floating_ip_pool" {
  description = "(Optional) Floating IP pool to use for all worker nodes"
  type        = string
  default     = "ext-net"
}
variable "ssh_keys" {
  description = "(Optional) ssh_key for ssh user"
  type        = string
  default     = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDP70R7TEf62SMFSsZPSJqCoZdpcg69oHfpiCpGCJYaB1mKyL5sBEnbKOWKkxBrHib+EOIp/Q/L9mllDxjxStF+fK7Xyd6jecxedQkkwLoSS1Yg981fzeSZC36lyiHZx9XUhuZ/0dGkifnguFp6mtkkDJu/7Q4KFwggnMkWmW1ypItHBI8YNQa4qYoIRTnszA6mO/ad9q8ichw3Vsr2QrNyErUXJYgwfyuGX3n+NZgUDqBw29hxPwrjdGnyptO53hNZu/D/gzOYKbxwREMhhVbK0kxZe/erBr+0niZVjEMDMupS7fdpyp/0brwcrOn30690SBsmPp0EGp7tYJMaBLWrEVReVeahJAID2DHxcuGyE1WGm0creL7kFoHbsRCPiM26dRnEYJolCOifsXfxB53Hee8+VU8YYWUGxOYfj2FagQHHf33TTAF61IKVvDk/Go+h+PLRoSiZKBlIS8TQPJSZhhPniS44sLTsjBuMaL5EbW0vC5UchXH2OOR2zj35LATp06EBL6qmdQNjYB/Fe8MBThmnFK5rSHVcEWX1rXV+iSiuneuwrqOidRUAiTUR90tdqySZZLh/0rswnNLcPVZnX50GX5Iaz9IxcBtbzOPaHfmaw2sJKpJne2eK3gJBh1fs3vLs5Nhv4yajLBP7cuRlqkkmZS/5z/97fyImpngy+Q=="
}
variable "ssh_username" {
  description = "(Optional) Name of the ssh user"
  type        = string
  default     = "lighton"
}
variable "kubernetes_version" {
  description = "(Optional) Supported K8S Version by Syseleven"
  type        = string
  default     = "1.27.10"
}
variable "metakube_project_id" {
  description = "(Required) Reference project identifier"
  type        = string
}
variable "openstack_networking_secgroup" {
  description = "(Required) Security Group for the cluster"
  type        = string
}
variable "openstack_networking_network" {
  description = "(Required) Network for the cluster nodes"
  type        = string
}
variable "openstack_networking_subnet" {
  description = "(Required) OpenStack Subnet"
  type        = string
}
variable "openstack_networking_secgroup_rule" {
  description = "(Required) Security Group for the Node Ports"
  type        = string
}
variable "openstack_compute_servergroup" {
  description = "(Required) OpenStack Server Group Anti affinity"
  type        = string
}
variable "audit_logging_enabled" {
  description = "(Optional) Enable AUDIT Logging"
  type        = bool
  default     = true
}
variable "pod_node_selector" {
  description = "(Optional) Enable POD Node Selector"
  type        = bool
  default     = false
}
variable "metakube_services_cidr" {
  description = "(Optional) CIDR for the Kubernetes Services"
  type        = string
  default     = "10.240.16.0/20"
}
variable "pods_cidr" {
  description = "(Optional) CIDR for the Kubernetes PODS"
  type        = string
  default     = "172.25.0.0/16"
}
variable "node_min_replicas" {
  description = "(Optional) Minimal Node Count"
  type        = number
  default     = 2
}
variable "node_max_replicas" {
  description = "(Optional) Maximal Node Count"
  type        = number
  default     = 5
}
variable "node_flavor" {
  description = "(Required) Flavor to use"
  type        = string
}
variable "node_disk_size" {
  description = "(Optional) Size of the HDD for the nodes"
  type        = number
  default     = 50
}
variable "node_image" {
  description = "(Required) The image for the nodes"
  type        = string
}
variable "use_floating_ip" {
  description = "(Optional) Use Floating IP for the nodes"
  type        = bool
  default     = true
}
variable "instance_ready_check_period" {
  description = "(Optional) Check instance after seconds"
  type        = string
  default     = "10s"
}
variable "instance_ready_check_timeout" {
  description = "(Optional) Timeout of the instance check"
  type        = string
  default     = "200s"
}
variable "syseleven_auth_realm" {
  description = "(Optional) Syseleven Auth Realm"
  type        = string
  default     = "ccsolutions"
}
variable "openstack_images_distro" {
  description = "(Required) The image name for the nodes"
  type        = string
}
variable "openstack_images_version" {
  description = "(Required) The image version for the nodes"
  type        = string
}
variable "cluster_create_timeouts" {
  description = "(Optional) Timeout for the cluster creation"
  type        = string
  default     = "2h"
}
variable "cluster_update_timeouts" {
  description = "(Optional) Timeout for the cluster update"
  type        = string
  default     = "2h"
}
variable "cluster_delete_timeouts" {
  description = "(Optional) Timeout for the cluster deletion"
  type        = string
  default     = "2h"
}
variable "node_pool_create_timeouts" {
  description = "(Optional) Timeout for the node pool creation"
  type        = string
  default     = "4h"
}
variable "node_pool_update_timeouts" {
  description = "(Optional) Timeout for the node pool update"
  type        = string
  default     = "4h"
}
variable "node_pool_delete_timeouts" {
  description = "(Optional) Timeout for the node pool deletion"
  type        = string
  default     = "4h"
}
variable "dist_upgrade_on_boot" {
  description = "(Optional) Upgrade the OS on boot"
  type        = bool
  default     = true
}
variable "external_labels" {
  description = "(Optional) Additional labels to be merged with the default cluster labels"
  type        = map(string)
  default     = {}
}